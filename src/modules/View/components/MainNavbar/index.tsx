import React from 'react';
import Logo from '../Logo';
import { Link } from 'react-router-dom';
import {Navbar} from 'react-bootstrap'

const MainNavbar = () => (
            <Navbar expand="sm" className="bg-light navbar navbar-expand-lg navbar-custom">
            <div className="container">
              <Logo />
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                <Link className="nav-link" to="/Recipes/new">Add Recipe</Link>
                </li>
            </ul>
            </Navbar.Collapse>
            </div>
          </Navbar>
)


export default MainNavbar