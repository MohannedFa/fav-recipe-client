import React from 'react'
import { Modal, Button } from "react-bootstrap"
import { CustomModalProps } from './types';

const CustomModal: React.FC<CustomModalProps> = ({ show, handleClose, body, error, buttons }) => (
    <Modal show={show} onHide={handleClose}>
    <Modal.Body className={error ? 'text-danger' : ''}>{body}</Modal.Body>
    <Modal.Footer>
      {buttons ? buttons : (
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      )}
    </Modal.Footer>
  </Modal>
);

export default CustomModal
