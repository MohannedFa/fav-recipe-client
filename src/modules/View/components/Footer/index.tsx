import React from 'react';


const Footer = () => {
        return(
            <footer className="py-5 bg-black">
            <div className="container">
              <p className="m-0 text-center text-white small">Copyright &copy; Recipe Manager {new Date().getFullYear()}</p>
            </div>
          </footer>

        )
}

export default Footer