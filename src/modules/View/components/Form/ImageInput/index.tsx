import React, { createRef } from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import './index.css'
import { ImageInputProps } from './types';

const ImageInput: React.FC<ImageInputProps> = ({ label, src, value, onChange }) => {
    const fileInputRef: any = createRef()
    return (
        <div className="form-group row">
            <div className="col-xs-12 col-sm-2">
                <label>{label}</label>
            </div>
            <div className="col-xs-12 col-sm-2">
                <div className="image-container" onClick={() => fileInputRef.current.click()}>
                    <input type="file" ref={fileInputRef} onChange={onChange} />
                    {src || value ? <img src={value ? URL.createObjectURL(value) : src} alt="dish" className="img-thumbnail" /> : (<div className="py-2"><FontAwesomeIcon icon={faPlus} size={'1x'} color="rgba(0, 0, 0, 0.5)" /><p>upload</p></div>)}
                </div>
            </div>
        </div>
    )
}


export default ImageInput