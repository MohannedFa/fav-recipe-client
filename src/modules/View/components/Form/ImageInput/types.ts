export interface ImageInputProps {
    label: string
    value?: string | null
    src?: string
    onChange: (event: any) => void
}