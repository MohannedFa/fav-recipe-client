export interface ListInputProps {
    label: string
    onChange: (value: string[]) => void
    addInput: () => void
    removeInput: () => void
    inputs: string[]
}