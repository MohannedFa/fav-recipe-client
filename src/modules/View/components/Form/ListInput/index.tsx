import React, { useState, useEffect } from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus, faMinus} from '@fortawesome/free-solid-svg-icons';
import './index.css'
import { ListInputProps } from './types'

const ListInput: React.FC<ListInputProps> = ({ label, inputs, addInput, removeInput, onChange }) => {

    const [list, setList] = useState(inputs)

    useEffect(() => {
        setList(inputs)
    }, [inputs])

    const onInputChange = (value: string, index: number) => {
        setList(prevValue => {
            prevValue[index] = value
            return prevValue
        })
        onChange(list)
    }

    const textInputs = inputs.map((input, index) => (
        <input type="text" value={input} key={index} onChange={(event => onInputChange(event.target.value, index))}  className="form-control mb-2" />
    ))

    return (
        <div className="form-group row">
            <div className="col-xs-12 col-sm-2">
                <label>{label}</label>
            </div>
            <div className="col-xs-12 col-sm-6">
                {textInputs}
            </div>
            <div className="col-xs-6 col-sm-2">
                <FontAwesomeIcon onClick={addInput} icon={faPlus} />
            </div>
            <div className="col-xs-12 col-sm-2">
                { inputs.length >= 2 &&  <FontAwesomeIcon onClick={removeInput} icon={faMinus} /> }
            </div>
        </div>
    )
}

export default ListInput