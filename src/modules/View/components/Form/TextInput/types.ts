export interface TextInputProps {
    label: string
    value: string
    onChange?: (event: any) => void
}