import React, { useState, useEffect } from 'react'
import ImageInput from '../../View/components/Form/ImageInput'
import ListInput from '../../View/components/Form/ListInput'
import TextArea from '../../View/components/Form/TextArea'
import { connect } from 'react-redux';
import TextInput from '../../View/components/Form/TextInput'
import './index.css'
import { createRecipe, recipeDismissError } from '../../../store/actions';
import { withRouter } from 'react-router-dom';
import CustomModal from '../../View/components/CustomModal';
import { AddRecipeProps } from './types';
import { recipeInput, recipeState } from '../../../store/types/recipes';
import {Dispatch} from 'redux'


const AddRecipe: React.FC<AddRecipeProps> = props => {
    const [recipeData, setRecipeData] = useState({
        title: '',
        ingredients: [''],
        recipe: '',
        image: undefined,
    })

    const { redirect } = props
    useEffect(() => {
        if (redirect) {
            props.history.push("/")
        }
    }, [redirect]) //eslint-disable-line
    const onItemChange = (value: any, item: string | number) => setRecipeData(preValue => ({ ...preValue, [item]: value }))
    const onAddIngredient = () => setRecipeData(prevValue => ({ ...prevValue, ingredients: prevValue.ingredients.concat('') }))
    const onRemoveIngredient = () => setRecipeData(prevValue => ({ ...prevValue, ingredients: prevValue.ingredients.slice(0, prevValue.ingredients.length - 1) }))
    const onSubmit = (event: any) => {
        event.preventDefault()
        props.onCreateRecipe(recipeData)
    }

    return(
        <React.Fragment>
            <CustomModal show={props.error} body={props.error} handleClose={props.onDismissError} error/>
            <div className="w-50 mx-auto">
                <div className="my-5">
                    <h2 className="text-secondary text-uppercase">ADD NEW RECIPE</h2>
                </div>
                <form onSubmit={(event) => event.preventDefault()}>
                    <TextInput label='title' value={recipeData.title} onChange={(event: any) => onItemChange(event.target.value, 'title')} />
                    <ListInput label="ingredients" onChange={(value: string[]) => onItemChange(value, 'ingredients')} addInput={() => onAddIngredient()} removeInput={() => onRemoveIngredient()} inputs={recipeData.ingredients} />
                    <TextArea label="recipe" rows={5} value={recipeData.recipe} onChange={(event: any) => onItemChange(event.target.value, 'recipe')} />
                    <ImageInput label="Dish Image" value={recipeData.image} onChange={(event: any) => onItemChange(event.target.files[0], 'image')} />
                    <button onClick={onSubmit} className="btn btn-secondary" disabled={props.isLoading} >Add {props.isLoading && <div className="spinner-border spinner-border-sm" role="status"></div>}</button>
                </form>
            </div>
        </React.Fragment>
    )
}

const mapStateToProps = (state: { recipes: recipeState }) => {
    return{
        isLoading: state.recipes.loading,
        redirect: state.recipes.redirect,
        error: state.recipes.error,
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        onCreateRecipe: (recipeData: recipeInput) => dispatch(createRecipe(recipeData)),
        onDismissError: () => dispatch(recipeDismissError()),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddRecipe))