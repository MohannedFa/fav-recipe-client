import { createRecipeType, recipeInput, recipesDismissErrorType } from "../../../store/types/recipes";

import {RouteComponentProps} from 'react-router-dom'

export interface AddRecipeProps extends RouteComponentProps{
    isLoading: boolean
    redirect: boolean
    error: null | string
    onCreateRecipe: (recipeData: recipeInput) => createRecipeType
    onDismissError: () => recipesDismissErrorType
}