import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { deleteRecipe, disableRedirect, getRecipes, recipeDismissError } from '../../../store/actions'
import { recipeState } from '../../../store/types/recipes'
import CustomModal from '../../View/components/CustomModal'
import ItemCard from '../../View/components/ItemCard'
import Spinner from '../../View/components/Spinner'
import { RecipeTableProps } from './types'

const Recipes: React.FC<RecipeTableProps> = props => {
    const {isFirstLoad} = props
    useEffect(() => {
        if (isFirstLoad) {
            props.onGetRecipes()
        }
    }, [isFirstLoad]) //eslint-disable-line
    useEffect(() => {
        props.onDisableRedirect()
    })
    const onDelete = (id: string) => {
        const approveDelete = window.confirm('Are you sure you want to delete this recipe?')
        if (approveDelete) {
            props.onDeleteRecipe(id)
        }
    }
    if (props.loading) return <Spinner />
    return(
        <React.Fragment>
            <CustomModal show={props.error} body={props.error} handleClose={props.onDismissError} error/>
            <div className="row my-5 mx-auto">
            {props.recipes.map(recipe => (
                <div key={recipe._id} className="col-xs-12 col-sm-3 my-3">
                        <ItemCard title={recipe.title} link={`/Recipes/${recipe._id}`} src={`http://localhost:4000${recipe.image}`} handleDelete={() => onDelete(recipe._id)} />
                </div>
            ))}
            </div>
        </React.Fragment>
    )
}

const mapStateToProps = (state: {recipes: recipeState}) => ({
    loading: state.recipes.loading,
    recipes: state.recipes.recipes,
    isFirstLoad: state.recipes.isFirstLoad,
    error: state.recipes.error,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    onGetRecipes: () => dispatch(getRecipes()),
    onDisableRedirect: () => dispatch(disableRedirect()),
    onDismissError: () => dispatch(recipeDismissError()),
    onDeleteRecipe: (id: string) => dispatch(deleteRecipe(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Recipes)