import { deleteRecipeType, disableRedirectType, getRecipesType, recipe, recipesDismissErrorType } from "../../../store/types/recipes";

export interface RecipeTableProps {
    loading: boolean
    recipes: recipe[],
    isFirstLoad: boolean,
    error: null | string,
    onGetRecipes: () => getRecipesType,
    onDisableRedirect: () => disableRedirectType,
    onDismissError: () => recipesDismissErrorType,
    onDeleteRecipe: (id: string) => deleteRecipeType,
}