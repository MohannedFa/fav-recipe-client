import React from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import { Dispatch } from 'redux'
import { disableRedirect } from '../../../store/actions'
import { recipeState } from '../../../store/types/recipes'
import NotFound from '../../NotFound'
import './index.css'
import { SingleRecipeProps } from './types'

const SingleRecipe: React.FC<SingleRecipeProps> = props => {
    const recipe = props.recipes.find(recipe => recipe._id === (props.match.params as {id: string}).id)
    if (!recipe) return <NotFound />
    return(
        <div className="mt-5 mx-auto w-75">
            <h1 className="my-3">{recipe.title}</h1>
            <img src={`http://localhost:4000${recipe.image}`} alt="dish" className="img-view" />
            <div className="my-3">
                {recipe.ingredients.map(ing => <div key={ing} className="badge badge-secondary mx-3">{ing}</div>)}
            </div>
            <p className="recipe">{recipe.recipe}</p>
            <Link to={`/recipes/edit/${recipe._id}`} className="btn btn-secondary">edit recipe</Link>
        </div>
    )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    onDisableRedirect: () => dispatch(disableRedirect()),
})

const mapStateToProps = (state: {recipes: recipeState}) => ({
    recipes: state.recipes.recipes,
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SingleRecipe))