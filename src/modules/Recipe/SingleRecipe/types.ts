import { RouteComponentProps } from "react-router-dom";
import { disableRedirectType, recipe } from "../../../store/types/recipes";

export interface SingleRecipeProps extends RouteComponentProps {
    onDisableRedirect: () => disableRedirectType
    recipes: recipe[]
}