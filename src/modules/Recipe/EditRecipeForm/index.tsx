import React, { useState, useEffect } from 'react'
import ImageInput from '../../View/components/Form/ImageInput'
import ListInput from '../../View/components/Form/ListInput'
import TextArea from '../../View/components/Form/TextArea'
import TextInput from '../../View/components/Form/TextInput'
import './index.css'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { editRecipe, recipeDismissError } from '../../../store/actions'
import CustomModal from '../../View/components/CustomModal'
import { EditRecipeProps } from './types'
import { recipeInput, recipeState } from '../../../store/types/recipes'
import { Dispatch } from 'redux'
import NotFound from '../../NotFound'


const EditRecipe: React.FC<EditRecipeProps> = props => {
    const recipe = props.recipes.find(recipe => recipe._id === (props.match.params as {id: string}).id)
    if (!recipe) return <NotFound />

    const [recipeData, setRecipeData] = useState({ //eslint-disable-line
        title: recipe.title,
        ingredients: [...recipe.ingredients],
        recipe: recipe.recipe,
        image: null,
    })

    const { redirect } = props
    useEffect(() => { //eslint-disable-line
        if (redirect) {
            props.history.push("/")
        }
    }, [redirect]) //eslint-disable-line

    const onItemChange = (value: any, item: string) => setRecipeData(preValue => ({ ...preValue, [item]: value }))
    const onAddIngredient = () => setRecipeData(prevValue => ({ ...prevValue, ingredients: prevValue.ingredients.concat('') }))
    const onRemoveIngredient = () => setRecipeData(prevValue => ({ ...prevValue, ingredients: prevValue.ingredients.slice(0, prevValue.ingredients.length - 1) }))
    const onSubmit = (event: any) => {
        event.preventDefault()
        props.onEditRecipe(recipe._id, recipeData)
    }


    return(
        <React.Fragment>
            <CustomModal show={props.error} body={props.error} handleClose={props.onDismissError} error/>
            <div className="w-50 mx-auto">
                <div className="my-5">
                    <h2 className="text-secondary text-uppercase">EDIT RECIPE</h2>
                </div>
                <form onSubmit={(event) => event.preventDefault()}>
                    <TextInput label='title' value={recipeData.title} onChange={(event) => onItemChange(event.target.value, 'title')} />
                    <ListInput label="ingredients" onChange={(value) => onItemChange(value, 'ingredients')} addInput={() => onAddIngredient()} removeInput={() => onRemoveIngredient()} inputs={recipeData.ingredients} />
                    <TextArea label="recipe" rows={5} value={recipeData.recipe} onChange={(event) => onItemChange(event.target.value, 'recipe')} />
                    <ImageInput label="Dish Image" src={'http://localhost:4000' + recipe.image} value={recipeData.image} onChange={(event) => onItemChange(event.target.files[0], 'image')} />
                    <button onClick={onSubmit} className="btn btn-secondary" disabled={props.isLoading} >Edit {props.isLoading && <div className="spinner-border spinner-border-sm" role="status"></div>}</button>
                </form>
            </div>
        </React.Fragment>
    )
}

const mapStateToProps = (state: { recipes: recipeState }) => {
    return {
        recipes: state.recipes.recipes,
        redirect: state.recipes.redirect,
        isLoading: state.recipes.loading,
        error: state.recipes.error,
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        onEditRecipe: (id: string, recipeData: recipeInput) => dispatch(editRecipe(id, recipeData)),
        onDismissError: () => dispatch(recipeDismissError()),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditRecipe))