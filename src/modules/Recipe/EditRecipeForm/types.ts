import { editRecipeType, recipeInput, recipesDismissErrorType, recipe } from "../../../store/types/recipes";

import {RouteComponentProps} from 'react-router-dom'

export interface EditRecipeProps extends RouteComponentProps{
    recipes: recipe[]
    isLoading: boolean
    redirect: boolean
    error: null | string
    onEditRecipe: (_id: string, recipeData: recipeInput) => editRecipeType
    onDismissError: () => recipesDismissErrorType
}

export interface EditRecipeFormState {
    title: string
    ingredients: string[]
    recipe: string
    image?: string | null,
}