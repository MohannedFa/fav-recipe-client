import * as actionTypes from '../actions/actionTypes'

export interface recipeState {
    recipes: recipe[]
    loading: boolean
    error: null | string
    redirect: boolean
    isEditing: boolean
    recipeData: null | recipe
    isFirstLoad: boolean
}

export interface recipe {
    _id: string
    title: string
    ingredients: string[]
    recipe: string
    image?: string | null
}

export interface recipeInput {
    title: string
    ingredients: string[]
    recipe: string
    image?: string | null
}

export interface createRecipeType {
    type: typeof actionTypes.CREATE_RECIPE
    recipeData: recipeInput
}

export interface createRecipeStartType {
    type: typeof actionTypes.CREATE_RECIPE_START
}

export interface createRecipeSuccessType {
    type: typeof actionTypes.CREATE_RECIPE_SUCCESS,
    recipeData: recipe
}

export interface createRecipeFailType {
    type: typeof actionTypes.CREATE_RECIPE_FAIL
    error: string
}

export interface recipesDismissErrorType {
    type: typeof actionTypes.RECIPES_DISMISS_ERROR
}

export interface disableRedirectType {
    type: typeof actionTypes.DISABLE_REDIRECT
}

export interface getRecipesType {
    type: typeof actionTypes.GET_RECIPES
}

export interface getRecipesStartType {
    type: typeof actionTypes.GET_RECIPES_START
}

export interface getRecipesSuccessType {
    type: typeof actionTypes.GET_RECIPES_SUCCESS
    recipes: recipe[]
}

export interface getRecipesFailType {
    type: typeof actionTypes.GET_RECIPES_FAIL
    error: string
}


export interface getRecipeType {
    type: typeof actionTypes.GET_RECIPE
    id: string
}


export interface getRecipeStartType {
    type: typeof actionTypes.GET_RECIPE_START
}

export interface getRecipeSuccessType {
    type: typeof actionTypes.GET_RECIPE_SUCCESS
    recipeData: recipe
}

export interface getRecipeFailType {
    type: typeof actionTypes.GET_RECIPE_FAIL
    error: string
}


export interface editRecipeType {
    type: typeof actionTypes.EDIT_RECIPE
    recipeData: recipeInput,
    id: string
}


export interface editRecipeStartType {
    type: typeof actionTypes.EDIT_RECIPE_START
}

export interface editRecipeSuccessType {
    type: typeof actionTypes.EDIT_RECIPE_SUCCESS
    recipeData: recipe
}

export interface editRecipeFailType {
    type: typeof actionTypes.EDIT_RECIPE_FAIL
    error: string
}

export interface deleteRecipeType {
    type: typeof actionTypes.DELETE_RECIPE
    id: string
}


export interface deleteRecipeStartType {
    type: typeof actionTypes.DELETE_RECIPE_START
}

export interface deleteRecipeSuccessType {
    type: typeof actionTypes.DELETE_RECIPE_SUCCESS
    recipeData: recipe
}

export interface deleteRecipeFailType {
    type: typeof actionTypes.DELETE_RECIPE_FAIL
    error: string
}

export type recipeAction =
createRecipeType | createRecipeStartType | createRecipeFailType | createRecipeSuccessType |
getRecipesType | getRecipesStartType | getRecipesSuccessType | getRecipesFailType |
getRecipeType | getRecipeStartType | getRecipeSuccessType | getRecipeFailType |
editRecipeType | editRecipeStartType | editRecipeFailType | editRecipeSuccessType |
deleteRecipeType | deleteRecipeStartType | deleteRecipeFailType | deleteRecipeSuccessType |
recipesDismissErrorType | disableRedirectType