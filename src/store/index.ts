import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import createReduxSagaMiddleware from 'redux-saga';
import recipesReducers from './reducers/recipes';
import { rootRecipeSaga } from './sagas'

declare global {
    interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}


const sagaMiddleware = createReduxSagaMiddleware();

const rootReducer = combineReducers({
    recipes: recipesReducers,
})

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(sagaMiddleware)
))

sagaMiddleware.run(rootRecipeSaga);

export default store