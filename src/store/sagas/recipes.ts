import { put } from 'redux-saga/effects'
import {
    createRecipeStart,
    createRecipeFail,
    createRecipeSuccess,
    editRecipeStart,
    editRecipeSuccess,
    editRecipeFail,
    getRecipesStart,
    getRecipesFail,
    getRecipesSuccess,
    deleteRecipeStart,
    deleteRecipeFail,
    deleteRecipeSuccess,
} from '../actions'
import * as types from '../types/recipes'
import axios from 'axios'

export function* createRecipeSaga(action: types.createRecipeType) {
    yield put(createRecipeStart())
    try{
        const formData = new FormData()
        formData.append('title', action.recipeData.title)
        const stringifiedIngredients = JSON.stringify(action.recipeData.ingredients)
        formData.append('ingredients', stringifiedIngredients)
        formData.append('recipe', action.recipeData.recipe)
        formData.append('image', action.recipeData.image!)
        const response = yield axios.post('http://localhost:4000/recipes', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        yield put(createRecipeSuccess(response.data.recipe))
    }catch(error){
        console.log('error', error)
        yield put(createRecipeFail(error.response.data.message))
    }
}

export function* getRecipesSaga(action: types.getRecipesType) {
    yield put(getRecipesStart())
    try {
        const response = yield axios.get('http://localhost:4000/recipes')
        yield put(getRecipesSuccess(response.data.recipes))
    } catch(error) {
        yield put(getRecipesFail(error.response.data.message))
    }
}

export function* editRecipeSaga(action: types.editRecipeType) {
    yield put(editRecipeStart())
    try{
        const formData = new FormData()
        formData.append('title', action.recipeData.title)
        const stringifiedIngredients = JSON.stringify(action.recipeData.ingredients)
        formData.append('ingredients', stringifiedIngredients)
        formData.append('recipe', action.recipeData.recipe)
        formData.append('image', action.recipeData.image!)
        const response = yield axios.put('http://localhost:4000/recipes/' + action.id, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        yield put(editRecipeSuccess(response.data.recipe))
    }catch(error){
        yield put(editRecipeFail(error.response.data.message))
    }
}

export function* deleteRecipeSaga(action: types.deleteRecipeType) {
    yield put(deleteRecipeStart())
    try{
        const response = yield axios.delete('http://localhost:4000/recipes/' + action.id)
        yield put(deleteRecipeSuccess(response.data.recipe))
    }catch(error) {
        yield put(deleteRecipeFail(error.response.data.message))
    }
}
