import { takeEvery } from "redux-saga/effects"
import { CREATE_RECIPE, DELETE_RECIPE, EDIT_RECIPE, GET_RECIPES } from "../actions/actionTypes"
import { createRecipeSaga, deleteRecipeSaga, editRecipeSaga, getRecipesSaga } from './recipes'

export function* rootRecipeSaga() {
    yield takeEvery(CREATE_RECIPE, createRecipeSaga)
    yield takeEvery(GET_RECIPES, getRecipesSaga)
    yield takeEvery(EDIT_RECIPE, editRecipeSaga)
    yield takeEvery(DELETE_RECIPE, deleteRecipeSaga)
}