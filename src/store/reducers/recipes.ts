import {
    CREATE_RECIPE_START,
    CREATE_RECIPE_SUCCESS,
    CREATE_RECIPE_FAIL,
    RECIPES_DISMISS_ERROR,
    EDIT_RECIPE_START,
    EDIT_RECIPE_SUCCESS,
    EDIT_RECIPE_FAIL,
    DISABLE_REDIRECT,
    GET_RECIPES_START,
    GET_RECIPES_SUCCESS,
    GET_RECIPES_FAIL,
    DELETE_RECIPE_START,
    DELETE_RECIPE_SUCCESS,
    DELETE_RECIPE_FAIL,
} from "../actions/actionTypes"
import { recipeAction, recipeState } from "../types/recipes"

const initialState : recipeState = {
    recipes: [],
    loading: false,
    error: null,
    redirect: false,
    isEditing: false,
    recipeData: null,
    isFirstLoad: true
}

const reducer = (state = initialState, action: recipeAction) => {
    switch (action.type) {
        case CREATE_RECIPE_START:
            return {
                ...state,
                loading: true,
            }
        case CREATE_RECIPE_SUCCESS:
            return {
                ...state,
                recipes: state.recipes.concat(action.recipeData),
                loading: false,
                redirect: true,
            }
        case CREATE_RECIPE_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error,
            }
        case RECIPES_DISMISS_ERROR:
            return {
                ...state,
                error: null,
            }
        case DISABLE_REDIRECT:
            return {
                ...state,
                redirect: false,
            }
        case GET_RECIPES_START:
            return {
                ...state,
                loading: true,
                recipes: [],
            }
        case GET_RECIPES_SUCCESS:
            return {
                ...state,
                loading: false,
                recipes: action.recipes,
                isFirstLoad: false,
            }
        case GET_RECIPES_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error,
            }
        case EDIT_RECIPE_START:
            return {
                ...state,
                loading: true,
            }
        case EDIT_RECIPE_SUCCESS:
            const recipeIndex = state.recipes.findIndex(rec => rec._id === action.recipeData._id)
            const newRecipes = [...state.recipes]
            newRecipes[recipeIndex] = action.recipeData
            return {
                ...state,
                recipes: newRecipes,
                loading: false,
                redirect: true,
            }
        case EDIT_RECIPE_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error,
            }
        case DELETE_RECIPE_START:
            return {
                ...state,
                loading: true,
            }
        case DELETE_RECIPE_SUCCESS:
            const index = state.recipes.findIndex(rec => rec._id === action.recipeData._id)
            const recipesCopy = [...state.recipes]
            recipesCopy.splice(index, 1)
            return {
                ...state,
                loading: false,
                recipes: recipesCopy,
            }
        case DELETE_RECIPE_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error,
            }
        default: return state
    }
}

export default reducer