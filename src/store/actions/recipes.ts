import * as types from "../types/recipes"
import {
    CREATE_RECIPE,
    CREATE_RECIPE_FAIL,
    CREATE_RECIPE_START,
    CREATE_RECIPE_SUCCESS,
    EDIT_RECIPE,
    EDIT_RECIPE_FAIL,
    EDIT_RECIPE_START,
    EDIT_RECIPE_SUCCESS,
    GET_RECIPES,
    GET_RECIPES_FAIL,
    GET_RECIPES_START,
    GET_RECIPES_SUCCESS,
    GET_RECIPE,
    GET_RECIPE_FAIL,
    GET_RECIPE_START,
    GET_RECIPE_SUCCESS,
    DELETE_RECIPE,
    DELETE_RECIPE_FAIL,
    DELETE_RECIPE_START,
    DELETE_RECIPE_SUCCESS,
    DISABLE_REDIRECT,
    RECIPES_DISMISS_ERROR,
} from "./actionTypes"

export const createRecipe: (recipeData: types.recipeInput) => types.createRecipeType = (recipeData) => {
    return {
        type: CREATE_RECIPE,
        recipeData,
    }
}

export const createRecipeStart: () => types.createRecipeStartType = () => {
    return {
        type: CREATE_RECIPE_START,
    }
}

export const createRecipeSuccess: (recipeData: types.recipe) => types.createRecipeSuccessType = (recipeData) => {
    return {
        type: CREATE_RECIPE_SUCCESS,
        recipeData,
    }
}

export const recipeDismissError: () => types.recipesDismissErrorType = () => {
    return {
        type: RECIPES_DISMISS_ERROR
    }
}


export const createRecipeFail: (error: string) => types.createRecipeFailType = error => {
    return {
        type: CREATE_RECIPE_FAIL,
        error,
    }
}

export const disableRedirect: () => types.disableRedirectType = () => {
    return {
        type: DISABLE_REDIRECT,
    }
}

export const editRecipe: (id: string, recipeData: types.recipeInput) => types.editRecipeType = (id, recipeData) => {
    return {
        type: EDIT_RECIPE,
        recipeData,
        id,
    }
}

export const editRecipeStart: () => types.editRecipeStartType = () => {
    return {
        type: EDIT_RECIPE_START,
    }
}

export const editRecipeSuccess: (recipeData: types.recipe) => types.editRecipeSuccessType = (recipeData) => {
    return {
        type: EDIT_RECIPE_SUCCESS,
        recipeData,
    }
}


export const editRecipeFail: (error: string) => types.editRecipeFailType = error => {
    return {
        type: EDIT_RECIPE_FAIL,
        error,
    }
}

export const getRecipes: () => types.getRecipesType = () => {
    return {
        type: GET_RECIPES,
    }
}

export const getRecipesStart: () => types.getRecipesStartType = () => {
    return {
        type: GET_RECIPES_START,
    }
}

export const getRecipesSuccess: (recipes: types.recipe[]) => types.getRecipesSuccessType = recipes => {
    return {
        type: GET_RECIPES_SUCCESS,
        recipes,
    }
}


export const getRecipesFail: (error: string) => types.getRecipesFailType  = error => {
    return {
        type: GET_RECIPES_FAIL,
        error,
    }
}

export const getRecipe: (id: string) => types.getRecipeType = (id) => {
    return {
        type: GET_RECIPE,
        id,
    }
}

export const getRecipeStart: () => types.getRecipeStartType = () => {
    return {
        type: GET_RECIPE_START,
    }
}

export const getRecipeSuccess: (recipeData: types.recipe) => types.getRecipeSuccessType = (recipeData) => {
    return {
        type: GET_RECIPE_SUCCESS,
        recipeData,
    }
}


export const getRecipeFail: (error: string) => types.getRecipeFailType = error => {
    return {
        type: GET_RECIPE_FAIL,
        error,
    }
}

export const deleteRecipe: (id: string) => types.deleteRecipeType = (id) => {
    return {
        type: DELETE_RECIPE,
        id,
    }
}

export const deleteRecipeStart: () => types.deleteRecipeStartType = () => {
    return {
        type: DELETE_RECIPE_START,
    }
}

export const deleteRecipeSuccess: (recipeData: types.recipe) => types.deleteRecipeSuccessType = (recipeData) => {
    return {
        type: DELETE_RECIPE_SUCCESS,
        recipeData,
    }
}


export const deleteRecipeFail: (error: string) => types.deleteRecipeFailType = error => {
    return {
        type: DELETE_RECIPE_FAIL,
        error,
    }
}
