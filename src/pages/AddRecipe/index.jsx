import React from 'react'
import AddRecipeForm from '../../modules/Recipe/AddRecipeForm'

export default function AddRecipePage() {
    return (
        <AddRecipeForm />
    )
}