import React from 'react'
import EditRecipeForm from '../../modules/Recipe/EditRecipeForm'


export default function EditRecipePage() {
    return (
        <EditRecipeForm />
    )
}