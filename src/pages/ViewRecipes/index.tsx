import React from 'react'
import RecipesTable from '../../modules/Recipe/RecipesTable'

export default function ViewRecipes() {
    return (
        <RecipesTable />
    )
}