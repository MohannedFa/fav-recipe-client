import React from 'react'
import SingleRecipe from '../../modules/Recipe/SingleRecipe'

export default function ViewRecipe() {
    return (
        <SingleRecipe />
    )
}