import React from 'react'
import { Switch, Route, withRouter } from 'react-router-dom'
import MainLayout from '../modules/View/Layout/MainLayout'
import NotFound from './NotFound'
import AddRecipe from './AddRecipe'
import EditRecipe from './EditRecipe'
import ViewRecipes from './ViewRecipes'
import ViewRecipe from './ViewRecipe'

const Pages = () =>  (
      <MainLayout className="App">
        <Switch>

          <Route path="/Recipes/edit/:id" component={EditRecipe} />
          <Route path="/Recipes/new" component={AddRecipe} />
          <Route path="/Recipes/:id" component={ViewRecipe} />
          <Route path="/" exact component={ViewRecipes} />

          <Route path="/" component={NotFound} />

        </Switch>
      </MainLayout>
);

export default withRouter(Pages)
